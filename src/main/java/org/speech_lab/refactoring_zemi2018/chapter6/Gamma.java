package org.speech_lab.refactoring_zemi2018.chapter6;

class Gamma {
  private Account _account;
  private int inputVal, quantity, yearToDate;
  private int importantValue1;
  private int importantValue2;
  private int importantValue3;

  public Gamma(Account source, int inputValArg, int quantityArg, int yearToDateArg){
    _account = source;
    inputVal = inputValArg;
    quantity = quantityArg;
    yearToDate = yearToDateArg;
  }

  private void importantThing(){
    if((yearToDate - importantValue1) > 100)
    importantValue2 -= 20;
  }

  public int compute(){
    int importantValue1 = (inputVal * quantity) + _account.delta();
    int importantValue2 = (inputVal * yearToDate) + 100;

    importantThing();

    importantValue2 *= 11;
    importantValue2 /= 10;

    importantThing();

    int importantValue3 = importantValue2 * 7;


    return importantValue3 - 2 * importantValue1;
  }
}
