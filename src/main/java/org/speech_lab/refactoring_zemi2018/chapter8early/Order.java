package org.speech_lab.refactoring_zemi2018.chapter8early;

class Order {
    private Customer _customer;

    public Order(String customer_name) {
        _customer = Customer.getBy(customer_name);
    }

    public String getCustomerName() {
        return _customer.getName();
    }

    public void setCustomer(String customer_name) {
        _customer = Customer.getBy(customer_name);
    }
}
