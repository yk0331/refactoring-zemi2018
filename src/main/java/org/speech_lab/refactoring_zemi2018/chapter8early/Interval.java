package org.speech_lab.refactoring_zemi2018.chapter8early;

import java.util.Observable;

class Interval extends Observable {
    String start = "0";
    String end = "0";
    String length = "0";

    void setStart(String arg){
        start = arg;
        setChanged();
        notifyObservers();
    }

    String getStart(){
        return start;
    }

    void setEnd(String arg){
        end = arg;
        setChanged();
        notifyObservers();
    }

    String getEnd(){
        return end;
    }

    void setLength(String arg){
        length = arg;
        setChanged();
        notifyObservers();
    }

    String getLength(){
        return length;
    }

    void calculateLength() {
        try {
            int start = Integer.parseInt(getStart());
            int end = Integer.parseInt(getEnd());
            int length = end - start;
            setLength(String.valueOf(length));
        } catch (NumberFormatException e) {
            throw new RuntimeException("予期しない数字形式のエラー");
        }
    }

    void calculateEnd() {
        try {
            int start = Integer.parseInt(getStart());
            int length = Integer.parseInt(getLength());
            int end = start + length;
            setEnd(String.valueOf(end));
        } catch (NumberFormatException e) {
            throw new RuntimeException("予期しない数字形式のエラー");
        }
    }
}
