package org.speech_lab.refactoring_zemi2018.chapter8early;

class IntRange {

    private int _low, _high;

    IntRange(int low, int high) {
        setLow(low);
        setHigh(high);
    }

    boolean includes(int arg) {
        return arg >= getLow() && arg <= getHigh();
    }

    void grow(int factor) {
        int new_high = getHigh() * factor;
        setHigh(new_high);
    }

    int getLow(){
        return _low;
    }

    void setLow(int low){
        _low = low;
    }

    int getHigh(){
        return _high;
    }

    void setHigh(int high){
        _high = high;
    }
}
