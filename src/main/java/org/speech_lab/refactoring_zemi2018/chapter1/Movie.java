package org.speech_lab.refactoring_zemi2018.chapter1;

public class Movie {
  private String _title;
  private Price _price;

  public Movie(String title, Price price){
    _title = title;
    _price = price;
  }

  public String getTitle(){
    return _title;
  }

  double getCharge(int daysRented){
    return _price.getCharge(daysRented);
  }

  int getFrequentRenterPoints(int daysRented){
    return _price.getFrequentRenterPoints(daysRented);
  }
}
