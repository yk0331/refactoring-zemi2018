package org.speech_lab.refactoring_zemi2018.chapter1;

import org.speech_lab.refactoring_zemi2018.chapter1.Movie;

public enum Price {
  REGULAR {
    @Override
    public double getCharge(int daysRented){
      double result = 2;
      if(daysRented > 2){
        result += (daysRented - 2) * 1.5;
      }
      return result;
    }

    @Override
    public int getFrequentRenterPoints(int daysRented){
      return 1;
    }
  },

  CHILDRENS {
    @Override
    public double getCharge(int daysRented){
      double result = 1.5;
      if(daysRented > 3){
        result += (daysRented - 3) * 1.5;
      }
      return result;
    }

    @Override
    public int getFrequentRenterPoints(int daysRented){
      return 1;
    }
  },

  NEW_RELEASE {
    @Override
    public double getCharge(int daysRented){
      return daysRented * 3;
    }

    // テキストでは記載ないが、
    // Overrideする場合アノテーションを付けるべき(*)なので追加
    // (*)Effective Javaのどこかにあったはず。あとで調べる。
    @Override
    public int getFrequentRenterPoints(int daysRented){

      // テキストでは3項演算子で記述されている
      // 今回は平易に書いておく
      if(daysRented > 1){
        return 2;
      } else {
        return 1;
      }
    }
  };

  public abstract double getCharge(int daysRented);
  public abstract int getFrequentRenterPoints(int daysRented);
}
