package org.speech_lab.refactoring_zemi2018.chapter8later;

public class Worker {
    private WorkerType _type;

    public static final int ENGINEER = 0;
    public static final int SALESMAN = 1;
    public static final int MANAGER = 2;

    public Worker() {
        super();
    }

    public Worker(int type) {
        _type = WorkerType.newType(type);
    }

    public void setType(int arg) {
        _type = WorkerType.newType(arg);
    }

    public int payAmount() {
        return _type.payAmount();
    }
}
