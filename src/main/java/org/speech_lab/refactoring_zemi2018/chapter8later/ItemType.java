package org.speech_lab.refactoring_zemi2018.chapter8later;

class ItemType {
    public static final ItemType BOOK = new ItemType(0);
    public static final ItemType DVD = new ItemType(1);
    public static final ItemType SOFT = new ItemType(2);

    private int _typecode;

    private ItemType(int typecode){
        _typecode = typecode;
    }

    int getTypecode(){
        return _typecode;
    }
}
