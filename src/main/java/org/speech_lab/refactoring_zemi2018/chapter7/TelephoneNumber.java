package org.speech_lab.refactoring_zemi2018.chapter7;

class TelephoneNumber {
    private String _areaCode;
    private String _number;

    public TelephoneNumber(){}

    public String getAreaCode(){
        return _areaCode;
    }

    public void setAreaCode(String arg){
        _areaCode = arg;
    }

    public String getNumber(){
        return _number;
    }

    public void setNumber(String arg){
        _number = arg;
    }

    public String toString(){
        return ("(" + _areaCode + ")" + _number);
    }
}
