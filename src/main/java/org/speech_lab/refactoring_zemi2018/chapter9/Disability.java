package org.speech_lab.refactoring_zemi2018.chapter9;

class Disability
{
    private int _seniorrity;
    private int _monthsDisabled;
    private boolean _isPartTime;


    public Disability(int seniorrity, int monthsDisabled, boolean isPartTime) {
        _seniorrity = seniorrity;
        _monthsDisabled = monthsDisabled;
        _isPartTime = isPartTime;
    }

    //-------------------------
    double disabilityAmount() {
        return isEligible() ? 200 : 0;
    }

    boolean isEligible(){
        return !(_seniorrity < 2 || _monthsDisabled > 12 || _isPartTime);
    }



    //--------------------------

}
