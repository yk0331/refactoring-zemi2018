# refactoring-zemi2018
- 研究室内リファクタリング勉強会のコード
- 資料など - [Link](https://scrapbox.io/wbydo-tech/slab%E3%83%AA%E3%83%95%E3%82%A1%E3%82%AF%E3%82%BF%E3%83%AA%E3%83%B3%E3%82%B0%E3%82%BC%E3%83%9F2018)

## 参考文献
Martin Fowler(2014)『リファクタリング : 既存のコードを安全に改善する』（児玉公信ほか訳）オーム社.

## 注意
ゼミ配布コードはantでビルドしてますが、このリポジトリはgradleでビルド出来る状態でpushしています。
